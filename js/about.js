const aboutvar = `<div class='container-fluid py-5'>
        <div class='container py-5'>
            <div class='row align-items-center'>
                <div class='col-lg-5'>
                    <img class='img-fluid rounded mb-4 mb-lg-0' src='img/about.jpg' alt=''>
                </div>
                <div class='col-lg-7'>
                    <div class='text-left mb-4'>
                        <h5 class='text-primary text-uppercase mb-3' style='letter-spacing: 5px;'>About Us</h5>
                        <h1>AI Predicted Quality by QualityWines</h1>
                    </div>
                    <p>Quality Wines is part of family of wines that are sold in more than 40 countries around the world under many different local names, including Wall's and Ola. Among its products are winess that are lower in fat and sugar, and enriched with fruit and calcium, which have been developed through the brand’s innovative food technologies. <br>In a world of stress, denial and restraint, providing moments of daily pleasure to consumers, through our delightfully delicious products, is our passion. We believe in spreading happiness and smiles through every cone, cup, stick and tub we sell. Our biggest satisfaction comes from the look of bliss and happiness of our consumers faces, as they devour our products. Our passion is inspired by our love for simple ingredients like Milk, Fruit and Chocolate, which make our products the best “Pleasure Food” there is. <br><br>Wine quality depends upon the composition of the grapes used in its production, which in turn depends on the weather and soil of the growing region together with viticultural practices. Region is used by many winemakers as a proxy for quality but objective quality measures are lacking. This study examined the compositional aspects of Chardonnay wines produced with berries from different regions. Through descriptive analysis, distinct sensory profiles were recognised for three diverse regions in South Australia (Adelaide Hills, Eden Valley, Riverland), which helped to pinpoint compounds relating to higher- and lower-quality Chardonnay wines. Correlations between the content of elements, fatty acids, free volatiles and conjugated glycosides in berries from different quality levels, and the composition of their corresponding wines, were investigated. Higher berry concentrations of linalool, (E)-linalool oxide, (Z)-3-hexen-1-ol, decanoic acid, vitispirane, Cu, Zn, and behenic acid, and lower °Brix and pH levels were related to higher quality wines.</p>
<a href='' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                </div>
            </div>
        </div>
    </div>
`
function about(){
document.getElementById("main").innerHTML=aboutvar;}
