const carouselH = `<div class='container-fluid p-0 pb-5 mb-5'>
        <div id='header-carousel' class='carousel slide carousel-fade' data-ride='carousel'>
            <ol class='carousel-indicators'>
                <li data-target='#header-carousel' data-slide-to='0' class='active'></li>
                <li data-target='#header-carousel' data-slide-to='1'></li>
                <li data-target='#header-carousel' data-slide-to='2'></li>
            </ol>
            <div class='carousel-inner'>
                <div class='carousel-item active' style='min-height: 300px;'>
                    <img class='position-relative w-100' src='img/wine1.jpg' style='min-height: 300px; object-fit: cover;'>
                    <div class='carousel-caption d-flex align-items-center justify-content-center'>
                        <div class='p-5' style='width: 100%; max-width: 900px;'>
                            <h5 class='text-white text-uppercase mb-md-3'>Goog Quality Wines Online</h5>
                            <h1 class='display-3 text-white mb-md-4'>Predict Wine Qualities From The Comfort of Your Home</h1>
                            <a href='' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                        </div>
                    </div>
                </div>
                <div class='carousel-item' style='min-height: 300px;'>
                    <img class='position-relative w-100' src='img/wine2.jpg' style='min-height: 300px; object-fit: cover;'>
                    <div class='carousel-caption d-flex align-items-center justify-content-center'>
                        <div class='p-5' style='width: 100%; max-width: 900px;'>
                            <h5 class='text-white text-uppercase mb-md-3'>AI Backed Intelligence</h5>
                            <h1 class='display-3 text-white mb-md-4'>Best Online Wine Quality Predicting Platform</h1>
                            <a href='' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                        </div>
                    </div>
                </div>
                <div class='carousel-item' style='min-height: 300px;'>
                    <img class='position-relative w-100' src='img/wine3.jpg' style='min-height: 300px; object-fit: cover;'>
                    <div class='carousel-caption d-flex align-items-center justify-content-center'>
                        <div class='p-5' style='width: 100%; max-width: 900px;'>
                            <h5 class='text-white text-uppercase mb-md-3'>Best Online Wines</h5>
                            <h1 class='display-3 text-white mb-md-4'>New Way To Predict Wine Qualities</h1>
                            <a href='javascript:services()' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`
const aboutvarH = `<div class='container-fluid py-5'>
        <div class='container py-5'>
            <div class='row align-items-center'>
                <div class='col-lg-5'>
                    <img class='img-fluid rounded mb-4 mb-lg-0' src='img/about.jpg' alt=''>
                </div>
                <div class='col-lg-7'>
                    <div class='text-left mb-4'>
                        <h5 class='text-primary text-uppercase mb-3' style='letter-spacing: 5px;'>About Us</h5>
                        <h1>AI Predicted Quality by QualityWines</h1>
                    </div>
                    <p>Quality Wines is part of family of wines that are sold in more than 40 countries around the world under many different local names, including Wall's and Ola. Among its products are winess that are lower in fat and sugar, and enriched with fruit and calcium, which have been developed through the brand’s innovative food technologies. <br>In a world of stress, denial and restraint, providing moments of daily pleasure to consumers, through our delightfully delicious products, is our passion. We believe in spreading happiness and smiles through every cone, cup, stick and tub we sell. Our biggest satisfaction comes from the look of bliss and happiness of our consumers faces, as they devour our products. Our passion is inspired by our love for simple ingredients like Milk, Fruit and Chocolate, which make our products the best “Pleasure Food” there is. <br><br>Wine quality depends upon the composition of the grapes used in its production, which in turn depends on the weather and soil of the growing region together with viticultural practices. Region is used by many winemakers as a proxy for quality but objective quality measures are lacking. This study examined the compositional aspects of Chardonnay wines produced with berries from different regions. Through descriptive analysis, distinct sensory profiles were recognised for three diverse regions in South Australia (Adelaide Hills, Eden Valley, Riverland), which helped to pinpoint compounds relating to higher- and lower-quality Chardonnay wines. Correlations between the content of elements, fatty acids, free volatiles and conjugated glycosides in berries from different quality levels, and the composition of their corresponding wines, were investigated. Higher berry concentrations of linalool, (E)-linalool oxide, (Z)-3-hexen-1-ol, decanoic acid, vitispirane, Cu, Zn, and behenic acid, and lower °Brix and pH levels were related to higher quality wines.</p>
<a href='javascript:services()' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                </div>
            </div>
        </div>
    </div>
`
const servicevarH=`<div class="container-fluid py-5">
        <div class="container py-5">
            <div class="text-center mb-5">
                <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">SERVICES</h5>
                <h1>Our Popular Services</h1>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/course-1.jpg" alt="">
                        <div class="bg-secondary p-4">
                            <div class="d-flex justify-content-between mb-3">
                                <small class="m-0"><i class="fa fa-users text-primary mr-2"></i>25 Students</small>
                                <small class="m-0"><i class="far fa-clock text-primary mr-2"></i>01h 30m</small>
                            </div>
                            <a class="h5" href="javascript:dtcdisplay()">Decision Tree based Prediction</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex justify-content-between">
                                    <h6 class="m-0"><i class="fa fa-star text-primary mr-2"></i>4.5 <small>(250)</small></h6>
                                    <h5 class="m-0">$10</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/course-2.jpg" alt="">
                        <div class="bg-secondary p-4">
                            <div class="d-flex justify-content-between mb-3">
                                <small class="m-0"><i class="fa fa-users text-primary mr-2"></i>25 Students</small>
                                <small class="m-0"><i class="far fa-clock text-primary mr-2"></i>01h 30m</small>
                            </div>
                            <a class="h5" href="javascript:rfcdisplay()">Random Forest Based Prediction</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex justify-content-between">
                                    <h6 class="m-0"><i class="fa fa-star text-primary mr-2"></i>4.5 <small>(250)</small></h6>
                                    <h5 class="m-0">$14.5</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="rounded overflow-hidden mb-2">
                        <img class="img-fluid" src="img/course-3.jpg" alt="">
                        <div class="bg-secondary p-4">
                            <div class="d-flex justify-content-between mb-3">
                                <small class="m-0"><i class="fa fa-users text-primary mr-2"></i>25 Students</small>
                                <small class="m-0"><i class="far fa-clock text-primary mr-2"></i>01h 30m</small>
                            </div>
                            <a class="h5" href="javascript:lgbmdisplay()">LGBM Classifier Based Prediction</a>
                            <div class="border-top mt-4 pt-4">
                                <div class="d-flex justify-content-between">
                                    <h6 class="m-0"><i class="fa fa-star text-primary mr-2"></i>4.5 <small>(250)</small></h6>
                                    <h5 class="m-0">$20</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
`
const devvarH=` <div class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <div class="text-center mb-5">
                <h5 class="text-primary text-uppercase mb-3" style="letter-spacing: 5px;">DEVELOPERS</h5>
                <h1>Meet Us</h1>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-1.jpg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>Vedant Bhatt</h5>
                            <p class="m-0">Sr. Pr. Data Scientist/AI Specialist at Amazon</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-2.jpeg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>Harvinder Singh</h5>
                            <p class="m-0">Sr. Pr. CloudDevSecOps Solutions Architect</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-3.jpg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>Balpinter Pal Singh</h5>
                            <p class="m-0">Farziwara and Murga Phaasna</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 text-center team mb-4">
                    <div class="team-item rounded overflow-hidden mb-2">
                        <div class="team-img position-relative">
                            <img class="img-fluid" src="img/team-4.jpg" alt="">
                            <div class="team-social">
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-outline-light btn-square mx-1" href="#"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                        <div class="bg-secondary p-4">
                            <h5>Niharika Dhanik</h5>
                            <p class="m-0">Sr. Pr. Cloud DevOps Engineer at Meta</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

`
const testimonialvarH = ` <div class='container-fluid py-5'>
        <div class='container py-5'>
            <div class='text-center mb-5'>
                <h5 class='text-primary text-uppercase mb-3' style='letter-spacing: 5px;'>Testimonial</h5>
                <h1>What Say Our Students</h1>
            </div>
            <div class='row justify-content-center'>
                <div class='col-lg-8'>
                    <div class='owl-carousel testimonial-carousel'>
                        <div class='text-center'>
                            <i class='fa fa-3x fa-quote-left text-primary mb-4'></i>
                            <h4 class='font-weight-normal mb-4'>I got the best results using the predictive methoddology of QualityWines</h4>
                            <img class='img-fluid mx-auto mb-3' src='img/testimonial-1.jpg' alt=''>
                            <h5 class='m-0'>Ransen Kumar Moto</h5>
                            <span>Founder & CEO of RansenTechs</span>
                        </div>
                        <div class='text-center'>
                            <i class='fa fa-3x fa-quote-left text-primary mb-4'></i>
                            <h4 class='font-weight-normal mb-4'>The best platform for predicting wine qualities I ever found</h4>
                            <img class='img-fluid mx-auto mb-3' src='img/testimonial-2.jpg' alt=''>
                            <h5 class='m-0'>Vickey Kumar Shahu</h5>
                            <span>Managing Director of RansenTechs</span>
                        </div>
                        <div class='text-center'>
                            <i class='fa fa-3x fa-quote-left text-primary mb-4'></i>
                            <h4 class='font-weight-normal mb-4'>Use It and you will never regret. QualityWines is just fantastic.</h4>
                            <img class='img-fluid mx-auto mb-3' src='img/testimonial-3.jpg' alt=''>
                            <h5 class='m-0'>Suraj Kumar shahu</h5>
                            <span>Future CTo of RansenTechs</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`




const home=carouselH+aboutvarH+servicevarH+devvarH+testimonialvarH;
function homepage(){
document.getElementById("main").innerHTML=home;}
