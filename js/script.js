const carousel = `<div class='container-fluid p-0 pb-5 mb-5'>
        <div id='header-carousel' class='carousel slide carousel-fade' data-ride='carousel'>
            <ol class='carousel-indicators'>
                <li data-target='#header-carousel' data-slide-to='0' class='active'></li>
                <li data-target='#header-carousel' data-slide-to='1'></li>
                <li data-target='#header-carousel' data-slide-to='2'></li>
            </ol>
            <div class='carousel-inner'>
                <div class='carousel-item active' style='min-height: 300px;'>
                    <img class='position-relative w-100' src='img/wine1.jpg' style='min-height: 300px; object-fit: cover;'>
                    <div class='carousel-caption d-flex align-items-center justify-content-center'>
                        <div class='p-5' style='width: 100%; max-width: 900px;'>
                            <h5 class='text-white text-uppercase mb-md-3'>Goog Quality Wines Online</h5>
                            <h1 class='display-3 text-white mb-md-4'>Predict Wine Qualities From The Comfort of Your Home</h1>
                            <a href='' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                        </div>
                    </div>
                </div>
                <div class='carousel-item' style='min-height: 300px;'>
                    <img class='position-relative w-100' src='img/wine2.jpg' style='min-height: 300px; object-fit: cover;'>
                    <div class='carousel-caption d-flex align-items-center justify-content-center'>
                        <div class='p-5' style='width: 100%; max-width: 900px;'>
                            <h5 class='text-white text-uppercase mb-md-3'>AI Backed Intelligence</h5>
                            <h1 class='display-3 text-white mb-md-4'>Best Online Wine Quality Predicting Platform</h1>
                            <a href='' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                        </div>
                    </div>
                </div>
                <div class='carousel-item' style='min-height: 300px;'>
                    <img class='position-relative w-100' src='img/wine3.jpg' style='min-height: 300px; object-fit: cover;'>
                    <div class='carousel-caption d-flex align-items-center justify-content-center'>
                        <div class='p-5' style='width: 100%; max-width: 900px;'>
                            <h5 class='text-white text-uppercase mb-md-3'>Best Online Wines</h5>
                            <h1 class='display-3 text-white mb-md-4'>New Way To Predict Wine Qualities</h1>
                            <a href='' class='btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2'>Predict Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`




const testimonial = ` <div class='container-fluid py-5'>
        <div class='container py-5'>
            <div class='text-center mb-5'>
                <h5 class='text-primary text-uppercase mb-3' style='letter-spacing: 5px;'>Testimonial</h5>
                <h1>What Say Our Students</h1>
            </div>
            <div class='row justify-content-center'>
                <div class='col-lg-8'>
                    <div class='owl-carousel testimonial-carousel'>
                        <div class='text-center'>
                            <i class='fa fa-3x fa-quote-left text-primary mb-4'></i>
                            <h4 class='font-weight-normal mb-4'>I got the best results using the predictive methoddology of QualityWines</h4>
                            <img class='img-fluid mx-auto mb-3' src='img/testimonial-1.jpg' alt=''>
                            <h5 class='m-0'>Client Name</h5>
                            <span>Profession</span>
                        </div>
                        <div class='text-center'>
                            <i class='fa fa-3x fa-quote-left text-primary mb-4'></i>
                            <h4 class='font-weight-normal mb-4'>The best platform for predicting wine qualities I ever found</h4>
                            <img class='img-fluid mx-auto mb-3' src='img/testimonial-2.jpg' alt=''>
                            <h5 class='m-0'>Client Name</h5>
                            <span>Profession</span>
                        </div>
                        <div class='text-center'>
                            <i class='fa fa-3x fa-quote-left text-primary mb-4'></i>
                            <h4 class='font-weight-normal mb-4'>Use It and you will never regret. QualityWines is just fantastic.</h4>
                            <img class='img-fluid mx-auto mb-3' src='img/testimonial-3.jpg' alt=''>
                            <h5 class='m-0'>Client Name</h5>
                            <span>Profession</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`


const registration = `<div class='container-fluid bg-registration py-5' style='margin: 90px 0;'>
        <div class='container py-5'>
            <div class='row align-items-center'>
                <div class='col-lg-7 mb-5 mb-lg-0'>
                    <div class='mb-4'>
                        <h5 class='text-primary text-uppercase mb-3' style='letter-spacing: 5px;'>Need Any Premium Services</h5>
                        <h1 class='text-white'>30% Off For Indian and Russian Clients</h1>
                    </div>
                    <p class='text-white'>Premium customer service exceeds basic courtesy, helpfulness, cashier and floor support. Customers typically get a more personalized service from a sales associate or customer service level</p>
                    <ul class='list-inline text-white m-0'>
                        <li class='py-2'><i class='fa fa-check text-primary mr-3'></i>Faster Quality Prediction</li>
                        <li class='py-2'><i class='fa fa-check text-primary mr-3'></i>24x7 Support and Assistance</li>
                        <li class='py-2'><i class='fa fa-check text-primary mr-3'></i>Home Visit Assistance</li>
                    </ul>
                </div>
                <div class='col-lg-5'>
                    <div class='card border-0'>
                        <div class='card-header bg-light text-center p-4'>
                            <h1 class='m-0'>Sign Up Now</h1>
                        </div>
                        <div class='card-body rounded-bottom bg-primary p-5'>
                            <form>
                                <div class='form-group'>
                                    <input type='text' class='form-control border-0 p-4' placeholder='Your name' required='required' />
                                </div>
                                <div class='form-group'>
                                    <input type='email' class='form-control border-0 p-4' placeholder='Your email' required='required' />
                                </div>
				<div class='form-group'>
                                    <input type='integer' class='form-control border-0 p-4' placeholder='Your Phone Number' required='required' />
                                </div>

                                <div>
                                    <button onclick='signup' class='btn btn-dark btn-block border-0 py-3' type='submit'>Sign Up Now</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`

