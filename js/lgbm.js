 const lgbmapi="https://www.qualitywines.pundhoro.ml/models";
    const lgbmvar = ` <div class='container-fluid py-5'>
        <div class='container py-5'>
            <div class='text-center mb-5'>
                <h5 class='text-primary text-uppercase mb-3' style='letter-spacing: 5px;'>LGBM Classifier</h5>
                <h1>Predict Your Wine Quality</h1>
            </div>
            <div class='row justify-content-center'>
                <div class='col-lg-8'>
                    <div class='contact-form bg-secondary rounded p-5'>
                        <div id='success'></div>
                        <form name='modelForm' id='modelFormId' action="javascript:predictlgbm()" novalidate='novalidate'>
			    <div><h4>Please Enter The Attributes</h4></div>
                            <div class='control-group'>
                                <input  name='fa' type='number' step='0.1' min='0' max='15' class='form-control border-0 p-4' id='fa' placeholder='Fixed Acidity eg. 7.6' required='required' data-validation-required-message='Please enter the value of fixed acidity between 0 to 15' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='va' type='number' step='0.1' min='0' max='2' class='form-control border-0 p-4' id='va' placeholder='Volatile Acidity eg. 0.65' required='required' data-validation-required-message='Please enter the value of volatile acidity between 0 to 2' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='ca' type='number' step='0.1' min='0' max='2' class='form-control border-0 p-4' id='ca' placeholder='Citric Acid eg. 0.06' required='required' data-validation-required-message='Please enter the value of citric acid between 0 to 2' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='rs' type='number' step='0.1' min='0' max='67' class='form-control border-0 p-4' id='rs' placeholder='Residual Sugar eg. 1.2' required='required' data-validation-required-message='Please enter the value of residual sugar between 0 to 67' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='cl' type='number' step='0.1' min='0' max='2' class='form-control border-0 p-4' id='cl' placeholder='Chlorides eg. 53.78' required='required' data-validation-required-message='Please enter the value of chlorides between 0 to 2' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='so2' type='number' step='0.1' min='0' max='289' class='form-control border-0 p-4' id='so2' placeholder='Free Sulphur Dioxide  eg. 14.99' required='required' data-validation-required-message='Please enter the value of free sulphur dioxide between 0 to 289' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='tso2' type='number' step='0.1' min='0' max='440' class='form-control border-0 p-4' id='tso2' placeholder='Total Sulphur Dioxide eg. 21' required='required' data-validation-required-message='Please enter the value of total sulphur dioxide between 0 to 440' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='den' type='number' step='0.1' min='0' max='2' class='form-control border-0 p-4' id='den' placeholder='Density eg. 0.9946' required='required' data-validation-required-message='Please enter the value of density between 0 to 2' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='ph' type='number' step='0.1' min='0' max='4' class='form-control border-0 p-4' id='ph' placeholder='pH eg. 3.39' required='required' data-validation-required-message='Please enter the value of pH between 0 to 4' />
                                <p class='help-block text-danger'></p>
                            </div>
                            <div class='control-group'>
                                <input name='so4' type='number' step='0.1' min='0' max='2' class='form-control border-0 p-4' id='so4' placeholder='Sulphates eg. 0.47' required='required' data-validation-required-message='Please enter the value of sulphates between 0 to 2' />
                                <p class='help-block text-danger'></p>
                            </div>      
                            <div class='control-group'>
                                <input name='oh'  type='number' step='0.1' min='0' max='15' class='form-control border-0 p-4' id='oh' placeholder='Alcohol eg. 13.69' required='required' data-validation-required-message='Please enter the value of alcohol between 0 to 15' />
                                <p class='help-block text-danger'></p>
                            </div>                      


                            <div class='text-center'>
                                <button id='modelButtonId' type="submit" class='btn btn-primary py-3 px-5' >PREDICT</button>
                            </div>
                            <div id="result"></div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div> 
                            


`

function lgbmdisplay(){
document.getElementById("main").innerHTML=lgbmvar;
}
function predictlgbm(){
 
  let formData = new FormData(document.forms.modelForm);
  let xhr = new XMLHttpRequest();
  xhr.open("POST",`${lgbmapi}`);
  xhr.send(formData);
  xhr.onload = function() {
  if (xhr.status != 200) { 
    document.getElementById('result').innerHTML=`Error ${xhr.status}: ${xhr.statusText}`; 
  } else { 
    document.getElementById("result").innerHTML=xhr.response;
  }
};
xhr.onerror = function() {
  document.getElementById('result').innerHTML=`<strong>Server Unreachable. Please Try After Sometime<strong>`;
};
}
